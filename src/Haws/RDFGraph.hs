module Haws.RDFGraph where

import Haws.MGraph
import Haws.MContext
import Haws.FGLMGraph
import Haws.Unify

data Resource = IRI String
              | Lit String
              | BNode BNodeId
              deriving (Show, Eq, Ord)

data RDFGraph = Basic (FGLMGraph Resource)
              | Exists (BNodeId -> RDFGraph)
             
instance Show RDFGraph where 
 show g = showRDF 1 g
 
showRDF :: BNodeId -> RDFGraph -> String
showRDF _ (Basic g) = 
  "\n" ++ foldr (\t r -> (show t) ++ "\n" ++ r) "" (triples g)
showRDF n (Exists f) = "Exists " ++ show n ++ " (" ++ showRDF (n + 1) (f n) ++ ")"

emptyRDF :: RDFGraph
emptyRDF = Basic empty

insertTripleRDF :: (Resource, Resource, Resource) -> RDFGraph -> RDFGraph
insertTripleRDF (s,p,o) (Basic g)  = Basic (insertTriple (s,p,o) g)
insertTripleRDF (s,p,o) (Exists f) = Exists (\bnode -> insertTripleRDF (s,p,o) (f bnode))

insertTriplesRDF :: [(Resource,Resource,Resource)] -> RDFGraph -> RDFGraph
insertTriplesRDF ts g = foldr insertTripleRDF g ts

triplesRDF :: RDFGraph -> [(Resource,Resource,Resource)]
triplesRDF g = triplesRDFfrom 0 g

triplesRDFfrom :: Int -> RDFGraph -> [(Resource,Resource,Resource)]
triplesRDFfrom _ (Basic g) = triples g
triplesRDFfrom n (Exists f) = triplesRDFfrom (n + 1) (f n)            

mergeRDF :: RDFGraph -> RDFGraph -> RDFGraph
mergeRDF g (Exists f) = Exists (\x -> mergeRDF g (f x))
mergeRDF g (Basic g1) = insertTriplesRDF (triples g1) g

containsIRI :: RDFGraph -> Resource -> Bool
containsIRI (Exists f) iri@(IRI _) = containsIRI (f 0) iri
containsIRI (Basic g) iri@(IRI _) = contains iri g
containsIRI _ _ = error "containsIRI only accepts IRIs"

foldRDFGraphOrd :: a -> (MContext Resource -> a -> a) -> RDFGraph -> a
foldRDFGraphOrd e h = foldRDFGraphOrdAux e h 0

foldRDFGraphOrdAux e h seed (Basic g) = foldMGraphOrd e h g 
foldRDFGraphOrdAux e h seed (Exists f)= foldRDFGraphOrdAux e h (seed + 1) (f seed)

foldRDFGraph :: a -> (MContext Resource -> a -> a) -> RDFGraph -> a
foldRDFGraph e h = foldRDFGraphAux e h 0

foldRDFGraphAux e h seed (Basic g)  = foldMGraph e h g 
foldRDFGraphAux e h seed (Exists f) = foldRDFGraphAux e h (seed + 1) (f seed)

printRDFFolds :: RDFGraph -> String
printRDFFolds = foldRDFGraphOrd "Empty" (\ctx r -> show ctx ++ "\n" ++ r)

type BNodeId = Int

type RDFContext = MContext Resource

isBNode :: RDFContext -> Bool
isBNode ctx = case node ctx of
 BNode _ -> True
 _       -> False


g1 :: RDFGraph
g1 = insertTripleRDF (IRI "1", IRI "2", IRI "3") emptyRDF 

g2 :: RDFGraph
g2 = Exists (\x -> insertTripleRDF (IRI "1", IRI "2", BNode x) emptyRDF )

g3 = Exists (\x -> insertTripleRDF (IRI "2", IRI "3", BNode x) g2)

g4 = Exists (\x -> Exists (\y -> 
        insertTripleRDF (IRI "a", IRI "b", BNode x)
        ( insertTripleRDF (IRI "a", IRI "b", BNode y)
        ( insertTripleRDF (BNode x, IRI "b", IRI "c") emptyRDF
        ))
      ))

g5 = Exists $ 
      \x -> Exists $
      \y -> insertTripleRDF (IRI "a", IRI "b", BNode x) $
            insertTripleRDF (BNode x, IRI "b", IRI "d") $
            insertTripleRDF (BNode x, IRI "b", IRI "e") $
            insertTripleRDF (IRI "a", IRI "b", IRI "c") $
            emptyRDF


