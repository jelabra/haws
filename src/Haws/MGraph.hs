{- | 'MGraph' provides an abstract representation of graphs were
   nodes and edges are of the same type and where edges can be nodes -} 
module Haws.MGraph(MGraph(..)) where

import Data.Maybe
import Data.Set

import Haws.MContext
import Haws.Subst
import Prelude hiding (pred,succ,map,null)
-- import Haws.Unify

class MGraph gr where 
   -- essential operations
   -- | An empty 'Graph'.
   gEmpty :: gr a 
 
   -- | Decompose a 'Graph' into the 'MContext' found for the given node and the
   -- remaining 'Graph'.
   -- This function is similar to 'match' in 'Data.Graph.Inductive' (we prefer the name 'decomp')
   decomp :: (Ord a, Show a) => a -> gr a -> Maybe (MContext a, gr a)

   -- | returns the nodes of the graph
   nodes :: (Ord a, Show a) => gr a -> Set a
   
   contains :: (Ord a, Show a) => a -> gr a -> Bool
   contains n g = member n (nodes g)
   
   insertTriple :: (Show a, Ord a) => (a,a,a) -> gr a -> gr a
 
   insertNode :: (Show a, Ord a) => a -> gr a -> gr a

   -- | 'decompAny' it decomposes a graph taking an arbitrary node (the head of 'nodes'). 
   --  It is similar 'matchAny' in 'Data.Graph.Inductive'
   decompAny :: (Ord a, Show a) => gr a -> Maybe (MContext a, gr a)
   decompAny g | null (nodes g) = Nothing
               | otherwise      = decomp (head (elems (nodes g))) g
                 
   -- | We could have let 'comp' as primitive and define 'insertTriple' in terms of 'comp'  
   comp :: (Ord a, Show a) => MContext a -> gr a -> gr a
   comp ctx gr =
    let n = node ctx
        gr0 = insertNode n gr
        gr1 = fold (\(p,x) g -> insertTriple (x,p,n) g) gr0 (pred ctx)
        gr2 = fold (\(p,y) g -> insertTriple (n,p,y) g) gr1 (succ ctx)
        gr3 = fold (\(x,y) g -> insertTriple (x,n,y) g) gr2 (rels ctx)
    in gr3   
   
   insertTriples :: (Show a, Ord a) => [(a,a,a)] -> gr a -> gr a
   insertTriples ts g = Prelude.foldr (\t r -> insertTriple t r) g ts
 
   foldMGraph :: (Show a, Show (gr a), Ord a) => b -> (MContext a -> b -> b) -> gr a -> b
   foldMGraph e h g = 
     if null (nodes g) then e
     else case decompAny g of
           Nothing -> error $ "foldMGraph: Cannot decomp graph " ++ show(g)
           Just (ctx,g') -> h ctx (foldMGraph e h g')
   
   foldMGraphOrd :: (Ord a, Show a, Show (gr a)) => b -> (MContext a -> b -> b) -> gr a -> b
   foldMGraphOrd e h g 
      | null (nodes g) = e
      | otherwise      = case decomp (findMin (nodes g)) g of
                   Nothing -> error $ "foldMGraphOrd: Node not found in graph " ++ show(g)
                   Just (ctx,g') -> h ctx (foldMGraph e h g')
                   

   showFolds :: (Show a, Ord a, Show (gr a), MGraph gr) => gr a -> String
   showFolds = foldMGraphOrd "empty\n" f
     where f :: (Show a) => MContext a -> String -> String 
           f ctx r = show ctx ++ "\n" ++ r
           
   triples :: (Show a, Show (gr a), Ord a) => gr a -> Set (a,a,a)
   triples = foldMGraph empty (\d r -> triplesCtx d `union` r)
   
   isEmpty :: (Ord a, Show a) => gr a -> Bool
   isEmpty gr = null (nodes gr) 

   context :: (MGraph gr, Ord a, Show a) => a -> gr a -> MContext a
   context n gr = case decomp n gr of
    Nothing -> error "Cannot get context"
    Just (ctx,_) -> ctx
   
   mapMGraph :: (Ord a, Show a, Show (gr a), 
                  Ord b, Show b, Show (gr b)) => (MContext a -> MContext b) -> gr a -> gr b
   mapMGraph f g = foldMGraph gEmpty (\ctx r -> comp (f ctx) r) g
        
   appSubstMGraph :: ( Ord a, Show a, VarCheck a, MGraph gr, Show (gr a)
                     ) => Subst a -> gr a -> gr a
   appSubstMGraph s = mapMGraph (appSubstCtx s)
     where appSubstCtx :: (Ord a, Show a, VarCheck a) => Subst a -> MContext a -> MContext a
           appSubstCtx s = mapCtx (\x -> case varCheck x of 
                                        Nothing -> x
                                        Just v  -> appSubst s v x)

   -- | reverse the edges in a graph
   grev :: (Show a, Ord a, Show (gr a)) => gr a -> gr a
   grev = mapMGraph swapCtx 
   
   
   -- | Generates an undirected graph from a graph (all links will be both directions)
   undir :: (Ord a, Show a, Show (gr a)) => gr a -> gr a
   undir = mapMGraph (\ctx -> let predSucc = pred ctx `union` succ ctx
                                  revRels = map (\(x,y) -> (y,x)) (rels ctx)
                              in ctx { pred = predSucc,
                                       succ = predSucc,
                                       rels = (rels ctx) `union` revRels
                                     })
                               
   -- | returns the successors of a node in a graph                                  
   gsuc :: (Ord a, Show a) => a -> gr a -> [a]
   gsuc n g = case decomp n g of
     Nothing -> []
     Just (ctx,g') -> toList (map snd (succ ctx))               
   
   -- | returns the successors of a node in a graph                                  
   deg :: (Ord a, Show a) => a -> gr a -> Int
   deg n g = case decomp n g of
     Nothing -> 0
     Just (ctx,g') -> size (succ ctx) + size (pred ctx) 

   -- | returns the successors of a node in a graph                                  
   delete :: (Ord a, Show a) => a -> gr a -> Maybe (gr a)
   delete n g = case decomp n g of
     Nothing -> Nothing
     Just (ctx,g') -> Just g'



-- This definition generates a decoposed graph...it is not necessary 
                    
data DecompMGraph a = EmptyMGraph
                    | Extend (MContext a) (DecompMGraph a)
               deriving Show

decompMGraph :: 
        ( Ord a, 
          Show a
        ) => MGraph gr => gr a -> DecompMGraph a
decompMGraph gr = 
        if isEmpty gr then EmptyMGraph
        else 
         let n = findMin (nodes gr)
         in case decomp n gr of
             Just (mctx,gr) -> Extend mctx (decompMGraph gr)
             Nothing -> error ("Not possible to decompose graph from node " ++ show n)
                    