module Haws.TestingExamples where

import Haws.MGraph
import Haws.GraphAlgorithms
import Haws.FGLMGraph
import Haws.MContext
import Data.Graph.Inductive
import Data.Graph.Inductive.Query.DFS
import Data.Set

b1 :: Gr Char Char
b1 = mkGraph [(1,'a'),(2,'b'),(3,'c')] 
            [(1,2,'p'),(2,1,'q'),(2,3,'r'),(3,1,'s')]

e :: FGLMGraph Char
e = gEmpty

mb1 :: FGLMGraph Char
mb1 = insertTriples 
          [('a','p','b'),
           ('b','q','a'),
           ('b','r','c'),
           ('c','s','a')]
           e

b2 :: Gr Char Char
b2 = mkGraph [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(5,'e')] 
            [(1,2,'p'),(2,1,'q'),(2,3,'r'),(3,1,'s'),(3,4,'t'),(3,5,'e'),(5,2,'b'),(1,3,'v')]

mb2 :: FGLMGraph Char
mb2 = insertTriples [('c','t','d'),
                     ('c','u','e'),
                     ('a','v','c'),
                     ('e','w','b')]
                     mb1

dfs1 = dfs [1] b1

dfs2 = dfs [1] b2

type Index = Int

data EValue a b = Node a | Edge b
 deriving (Show, Eq, Ord)

-- Simulate Erwig's graphs with my graphs
data EGraph a b = FGLMGraph (EValue a b)

ee :: FGLMGraph (EValue Char (String,Int))
ee = gEmpty

ee1 = insertTriples 
       [(Node 'A',Edge ("AG",90), Node 'G'),
        (Node 'A',Edge ("AB",20), Node 'B'),
        (Node 'B',Edge ("BF",10), Node 'F'),
        (Node 'C',Edge ("CD",10), Node 'D'),
        (Node 'C',Edge ("CH",20), Node 'H'),
        (Node 'C',Edge ("CF",50), Node 'F'),
        (Node 'D',Edge ("DG",20), Node 'G'),
        (Node 'E',Edge ("EB",50), Node 'B'),
        (Node 'E',Edge ("EG",30), Node 'G'),
        (Node 'F',Edge ("FC",10), Node 'C'),
        (Node 'F',Edge ("FD",40), Node 'D')
       ]
      ee

data Tree a = Br a [Tree a]

postorderT :: Tree a -> [a]
postorderT (Br v ts) = concatMap postorderT ts ++ [v]