module Haws.FunMGraph where 
import Data.Maybe
import Data.Set

import Haws.MGraph
import Haws.MContext
import Prelude hiding (succ,pred)

-- A basic generic graph is a list of nodes and a function from nodes to contexts 
data FunMGraph a = FunMGraph (a -> Maybe (MContext a, FunMGraph a)) (Set a)

instance Show a => Show (FunMGraph a) where 
 show = showFunMGraph  

instance MGraph FunMGraph where 
  gEmpty = emptyFunMGraph
  decomp = decompFun 
  nodes = nodesFun
  insertTriple = insertTripleFunMGraph
  insertNode = insertNodeFun

showFunMGraph :: (Show a) => FunMGraph a -> String
showFunMGraph (FunMGraph f ns) = 
  fold (\n r -> show (fst (fromJust (f n))) ++ "\n" ++ r) [] ns

emptyFunMGraph :: FunMGraph a
emptyFunMGraph = FunMGraph (\_ -> Nothing) empty

decompFun :: a -> FunMGraph a -> Maybe (MContext a, FunMGraph a)
decompFun n (FunMGraph f _) = f n 

nodesFun :: FunMGraph a -> Set a
nodesFun (FunMGraph _ ns) = ns


insertNodeFun :: (Show a, Ord a) => a -> FunMGraph a -> FunMGraph a
insertNodeFun e g@(FunMGraph f ns) = 
        if contains e g then g
        else FunMGraph (\n -> 
                if n == e then 
                  Just (Ctx {node = n,pred = empty,succ = empty, rels = empty},
                        remove n g)
                else f n) 
        (insert e ns)


insertEdgeCtx :: (Show a, Ord a) => (a,a,a) -> FunMGraph a -> a -> Maybe (MContext a, FunMGraph a)
insertEdgeCtx (x,p,y) g@(FunMGraph f _) n 
  | n == x = Just (mergeSucc (p,y) (context x g), remove x g)
  | n == p = Just (mergeRels (x,y) (context p g), remove p g)
  | n == y = Just (mergePred (x,p) (context y g), remove y g)
  | otherwise = f n 

remove :: (Show a, Ord a) => a -> FunMGraph a -> FunMGraph a
remove e (FunMGraph f xs) = 
        FunMGraph (\n -> if n == e then Nothing
                     else case f n of 
                        Just (ctx,g) -> Just (deleteNodeCtx e ctx, remove e g)
                        Nothing -> Nothing) 
              (delete e xs)

insertTripleFunMGraph :: (Show a, Ord a) => (a,a,a) -> FunMGraph a -> FunMGraph a
insertTripleFunMGraph (x,p,y) g = 
        let g1 = insertNodeFun x g
            g2 = insertNodeFun p g1
            g3 = insertNodeFun y g2
        in insertEdge (x,p,y) g3

insertEdge :: (Show a, Ord a) => (a,a,a) -> FunMGraph a -> FunMGraph a
insertEdge (x,p,y) g@(FunMGraph _ ns) =
        FunMGraph (insertEdgeCtx (x,p,y) g) ns


-- ex 1 g(1,2,3)
ex1 :: FunMGraph Int
ex1 = FunMGraph (\n -> case n of
  1 -> Just (Ctx {node = 1,pred = fromList [(2,3)],succ = empty,rels = empty},gEmpty)
  2 -> Just (Ctx {node = 2,pred = empty,succ = empty,rels = fromList [(1,3)]},gEmpty)
  3 -> Just (Ctx {node = 3,pred = empty,succ = fromList [(1,2)],rels = empty},gEmpty)
  _ -> Nothing)
  (fromList [1,2,3])

ex2 :: FunMGraph Int
ex2 = insertNodeFun 4 ex1
