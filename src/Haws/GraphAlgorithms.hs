module Haws.GraphAlgorithms where

import Haws.MGraph
import Haws.FGLMGraph
import Haws.MContext

import Data.Set

-- | dfs = depth first search
mdfs :: (Show a, MGraph gr, Ord a) => 
           [a] -> gr a -> [a]
mdfs [] _ = []
mdfs (v:vs) g = 
 if isEmpty g then []
 else 
  case decomp v g of 
    Nothing -> mdfs vs g
    Just (ctx,g') -> v : mdfs (toList (succNodes ctx) ++ vs) g'
    
-- | bfs = breadth first search
mbfs :: (Show a, MGraph gr, Ord a) => 
           [a] -> gr a -> [a]
mbfs [] _ = []
mbfs (v:vs) g = 
 if isEmpty g then []
 else 
  case decomp v g of 
    Just (ctx,g') -> v : mbfs (vs ++ toList(succNodes ctx)) g'
    Nothing -> mbfs vs g
    
    
    