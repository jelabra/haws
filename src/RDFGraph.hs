import Data.Maybe
import Data.List

{- Inductive definition of RDF Graphs using functions -}

-- A context contains a node, a list of succesors of that node, a list 
-- of the predecessors and a list of the nodes 
-- that it links (if the node is an edge)
data Context a = Ctx (a, [(a,a)], [(a,a)], [(a,a)])
 deriving Show

-- A basic generic graph is a list of nodes and a function from nodes to contexts 
data Graph a = Graph (a -> Maybe (Context a, Graph a)) [a]

instance Show a => Show (Graph a) where {
 show = showGraph  
}

empty :: Graph a
empty = Graph (\n -> Nothing) []

-- ex 1 g(1,2,3)
ex1 :: Graph Int
ex1 = Graph (\n -> case n of
  1 -> Just (Ctx (1,[(2,3)],[],[]),empty)
  2 -> Just (Ctx (2,[],[],[(1,3)]),empty)
  3 -> Just (Ctx (3,[],[(1,2)],[]),empty)
  _ -> Nothing)
  [1,2,3]

ex2 :: Graph Int
ex2 = insertNode 4 ex1

contains :: Graph a -> a -> Bool
contains (Graph f ns) n = isJust (f n)

nodes :: Graph a -> [a]
nodes (Graph f ns) = ns

context :: Graph a -> a -> Context a
context (Graph f ns) = fst . fromJust . f

decomp :: Eq a => Graph a -> a -> Maybe (Context a, Graph a)
decomp (Graph f ns) n = f n 

insertNode :: (Show a, Eq a ) => a -> Graph a -> Graph a
insertNode node g@(Graph f ns) = 
        if contains g node then g
        else Graph (\n -> if n == node then Just (Ctx (n,[],[],[]),remove n g)
                          else f n) 
                   (node:ns)

insertTriple :: (Show a, Eq a) => Graph a -> (a,a,a) -> Graph a
insertTriple g (x,p,y) = 
        let g1 = insertNode x g
            g2 = insertNode p g1
            g3 = insertNode y g2
        in insertEdge (x,p,y) g3

insertTriples :: (Show a, Eq a) => Graph a -> [(a,a,a)] -> Graph a
insertTriples g fs = foldr (\t r -> insertTriple r t) g fs

insertEdge :: (Show a, Eq a) => (a,a,a) -> Graph a -> Graph a
insertEdge (x,p,y) g@(Graph f ns) =
        Graph (insertEdgeCtx (x,p,y) g) ns
        
insertEdgeCtx :: (Show a, Eq a) => (a,a,a) -> Graph a -> a -> Maybe (Context a, Graph a)
insertEdgeCtx (x,p,y) g@(Graph f ns) n 
  | n == x = Just (mergeSucc  (p,y) (context g x), remove x g)
  | n == p = Just (mergeNodes (x,y) (context g p), remove p g)
  | n == y = Just (mergePred  (x,p) (context g y), remove y g)
  | otherwise = f n 
  
mergeSucc :: (a,a) -> Context a -> Context a
mergeSucc (p,y) (Ctx (n,succ,pred,nodes)) = Ctx (n,(p,y):succ,pred,nodes)
         
mergePred :: (a,a) -> Context a -> Context a
mergePred (x,p) (Ctx (n,succ,pred,nodes)) = Ctx (n,succ,(x,p):pred,nodes)

mergeNodes :: (a,a) -> Context a -> Context a
mergeNodes (x,y) (Ctx (n,succ,pred,nodes)) = Ctx (n,succ,pred,(x,y):nodes)

deleteNode :: (Show a, Eq a) => a -> Context a -> Context a
deleteNode node (Ctx (n,succ,pred,nodes)) = 
        if (n == node) then error ("Trying to delete node " ++ show node ++ " from its own context")
        else Ctx (n, 
                  filter(\(p,y) -> p /= node && y /= node) succ,
                  filter(\(x,p) -> p /= node && x /= node) pred,
                  filter(\(x,y) -> x /= node && y /= node) nodes)
                  
triplesCtx :: Context a -> [(a,a,a)]
triplesCtx (Ctx (n,succ,pred,nodes)) = 
        foldr (\(p,y) r -> (n,p,y):r) [] succ ++
        foldr (\(x,p) r -> (x,p,n):r) [] pred ++
        foldr (\(x,y) r -> (x,n,y):r) [] nodes       

remove :: (Show a, Eq a) => a -> Graph a -> Graph a
remove node (Graph f xs) = 
        Graph (\n -> if n == node then Nothing
                     else case f n of 
                        Just (ctx,g) -> Just (deleteNode node ctx, remove node g)
                        Nothing -> Nothing) 
              (delete node xs)

triples :: Graph a -> [(a,a,a)]
triples (Graph f []) = []
triples (Graph f (n:ns)) = 
        let (c,g) = fromJust (f n)
        in triplesCtx c ++ triples g

foldGraph :: Show a => Graph a -> b -> (Context a -> b -> b) -> b
foldGraph (Graph f []) e h = e
foldGraph g@(Graph f (n:ns)) e h = 
   case f n of 
    Just (ctx,g') -> h ctx (foldGraph g' e h)
    Nothing -> error ("FoldGraph: Node " ++ show(n) ++ " not found in graph " ++ show(g))            

foldSelGraph :: Show a => Graph a -> ([a] -> a) -> b -> (Context a -> b -> b) -> b
foldSelGraph (Graph f []) sel e h = e
foldSelGraph g@(Graph f ns) sel e h = 
   case f (sel ns) of 
    Just (ctx,g') -> h ctx (foldSelGraph g' sel e h)
    Nothing -> error ("FoldSelGraph: Node " ++ show (sel ns) ++ " not found in graph " ++ show(g))

showGraph :: Show a => Graph a -> String
showGraph (Graph f ns) = 
  foldr (\n r -> show (fst (fromJust (f n))) ++ "\n" ++ r) [] ns

printFolds :: (Show a, Ord a) => Graph a -> String
printFolds g = foldSelGraph g minimum "empty\n" f
  where f :: (Show a) => Context a -> String -> String 
        f (Ctx (a,succ,pred,nodes)) s = 
                "Node: " ++ show a ++
                ", Succ: " ++ show succ ++
                ", Pred: " ++ show pred ++
                ", Nodes: " ++ show nodes ++ "\n" ++
                s
                
--- RDF Graphs ...

data Resource = IRI String
              | Lit String
              | BNode BNodeId
              deriving (Show, Eq, Ord)

data RDFGraph = Basic (Graph Resource)
              | Exists (BNodeId -> RDFGraph)
             
instance Show RDFGraph where 
 show g = showRDF 1 g
 
showRDF :: BNodeId -> RDFGraph -> String
showRDF n (Basic g) = show g
showRDF n (Exists f) = showRDF (n + 1) (f n)

emptyRDF :: RDFGraph
emptyRDF = Basic empty

insertTripleRDF :: RDFGraph -> (Resource, Resource, Resource) -> RDFGraph
insertTripleRDF (Basic g)  (s,p,o) = Basic (insertTriple g (s,p,o))
insertTripleRDF (Exists f) (s,p,o) = Exists (\bnode -> insertTripleRDF (f bnode) (s,p,o))

insertTriplesRDF :: RDFGraph -> [(Resource,Resource,Resource)] -> RDFGraph
insertTriplesRDF g = foldr (\t r -> insertTripleRDF r t) g

triplesRDF :: RDFGraph -> [(Resource,Resource,Resource)]
triplesRDF g = triplesRDFfrom 0 g

triplesRDFfrom :: Int -> RDFGraph -> [(Resource,Resource,Resource)]
triplesRDFfrom n (Basic g) = triples g
triplesRDFfrom n (Exists f) = triplesRDFfrom (n + 1) (f n)            

mergeRDF :: RDFGraph -> RDFGraph -> RDFGraph
mergeRDF g (Exists f) = Exists (\x -> mergeRDF g (f x))
mergeRDF g (Basic g1) = insertTriplesRDF g (triples g1)

containsIRI :: RDFGraph -> Resource -> Bool
containsIRI (Exists f) iri@(IRI _) = containsIRI (f 0) iri
containsIRI (Basic g) iri@(IRI _) = contains g iri
containsIRI _ _ = error "containsIRI only accepts IRIs"

decompRDF :: RDFGraph -> Resource -> DecompRDFGraph
decompRDF (Basic g) iri = BasicDec (decomp g iri)
decompRDF (Exists f) iri = ExistsDec (\n -> decompRDF (f n) iri)

data DecompRDFGraph = BasicDec (Maybe (Context Resource, Graph Resource))
                    | ExistsDec (BNodeId -> DecompRDFGraph)


foldSelRDFGraph :: RDFGraph -> ([Resource] -> Resource) -> a -> (Context Resource -> a -> a) -> a
foldSelRDFGraph g sel e h = foldSelRDFGraphAux g sel e h 0

foldSelRDFGraphAux (Basic g) sel e h seed = foldSelGraph g sel e h  
foldSelRDFGraphAux (Exists f) sel e h seed = foldSelRDFGraphAux (f seed) sel e h (seed + 1)


foldRDFGraph :: RDFGraph -> a -> (Context Resource -> a -> a) -> a
foldRDFGraph g e h = foldRDFGraphAux g e h 0

foldRDFGraphAux (Basic g) e h seed = foldGraph g e h  
foldRDFGraphAux (Exists f) e h seed = foldRDFGraphAux (f seed) e h (seed + 1)

printSelFolds :: RDFGraph -> String
printSelFolds g = foldSelRDFGraph g minResource "Empty" f 
 where f :: Context Resource -> String -> String
       f (Ctx (node, succ, pred, nodes)) s = 
        "Node: " ++ show node ++ "\n" ++
        "succ: " ++ show(succ) ++ "\n" ++
        "pred: " ++ show(pred) ++ "\n" ++
        "nodes: " ++ show(nodes) ++ "\n" ++
        s

minResource :: [Resource] -> Resource
minResource [] = error "minResource: empty list"
minResource ls = head (sort ls)
 
printRDFFolds :: RDFGraph -> String
printRDFFolds g = foldRDFGraph g "Empty" f 
 where f :: Context Resource -> String -> String
       f (Ctx (node, succ, pred, nodes)) s = 
        "Node: " ++ show node ++ "\n" ++
        "succ: " ++ show(succ) ++ "\n" ++
        "pred: " ++ show(pred) ++ "\n" ++
        "nodes: " ++ show(nodes) ++ "\n" ++
        s
   
type BNodeId = Int

g1 :: RDFGraph
g1 = insertTripleRDF emptyRDF (IRI "1", IRI "2", IRI "3") 

g2 :: RDFGraph
g2 = Exists (\x -> insertTripleRDF emptyRDF (IRI "1", IRI "2", BNode x) )

g3 = Exists (\x -> insertTripleRDF g2 (IRI "2", IRI "3", BNode x))

g4 = Exists (\x -> Exists (\y -> 
        insertTripleRDF (
        insertTripleRDF (
        insertTripleRDF emptyRDF 
          (IRI "a", IRI "b", BNode x))
          (IRI "a", IRI "b", BNode y))
          (BNode x, IRI "b", IRI "c")
        ))
